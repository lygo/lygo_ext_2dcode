package generators

import (
	"bitbucket.org/lygo/lygo_commons/lygo_paths"
	"bitbucket.org/lygo/lygo_commons/lygo_reflect"
	"bitbucket.org/lygo/lygo_ext_2dcode/commons"
	"bytes"
	"github.com/yeqown/go-qrcode"
	"strings"
)

// uses go get -u github.com/yeqown/go-qrcode

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type GeneratorQrCode struct {
	format commons.BarcodeFormat
}

func NewGeneratorQrCode() *GeneratorQrCode {
	instance := new(GeneratorQrCode)
	instance.format = commons.BarcodeFormat_QR_CODE

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *GeneratorQrCode) Encode(contents string, options ...interface{}) ([]byte, error) {
	qrc, err := qrcode.New(contents, instance.getOptions(options)...)
	if nil != err {
		return nil, err
	}
	var buf bytes.Buffer
	err = qrc.SaveTo(&buf)
	if nil != err {
		return nil, err
	}
	return buf.Bytes(), nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *GeneratorQrCode) getOptions(options []interface{}) []qrcode.ImageOption {
	response := make([]qrcode.ImageOption, 0)
	for _, option := range options {
		option = lygo_reflect.ValueOf(option).Interface()
		if o, b := option.(commons.OptionSize); b {
			response = append(response, qrcode.WithQRWidth(uint8(o.Width)))
		} else if _, b := option.(commons.OptionCircleShapes); b {
			response = append(response, qrcode.WithCircleShape())
		} else if o, b := option.(commons.OptionImageLogo); b {
			f := o.Filename
			if strings.ToLower(lygo_paths.ExtensionName(f)) == "png" {
				response = append(response, qrcode.WithLogoImageFilePNG(f))
			} else if strings.ToLower(lygo_paths.ExtensionName(f)) == "jpg" {
				response = append(response, qrcode.WithLogoImageFileJPEG(f))
			}
		}
	}
	return response
}
