package generators

import (
	"bitbucket.org/lygo/lygo_ext_2dcode/commons"
	"github.com/makiuchi-d/gozxing"
	"github.com/makiuchi-d/gozxing/oned"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type GeneratorCode39 struct {
	format commons.BarcodeFormat
}

func NewGeneratorCode39() *GeneratorCode39 {
	instance := new(GeneratorCode39)
	instance.format = commons.BarcodeFormat_CODE_39

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *GeneratorCode39) Encode(contents string, options ...interface{}) ([]byte, error) {
	writer := oned.NewCode39Writer()
	return commons.Encode(writer, gozxing.BarcodeFormat_CODE_39, contents, options...)
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------
