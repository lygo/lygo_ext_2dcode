package scanners

import (
	"bitbucket.org/lygo/lygo_ext_2dcode/commons"
	"github.com/makiuchi-d/gozxing/oned"
)

//----------------------------------------------------------------------------------------------------------------------
//	t y p e s
//----------------------------------------------------------------------------------------------------------------------

type ScannerCode39 struct {
	format commons.BarcodeFormat
}

func NewScannerCode39() *ScannerCode39 {
	instance := new(ScannerCode39)
	instance.format = commons.BarcodeFormat_CODE_39

	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *ScannerCode39) Decode(filename string, options ...interface{}) (string, error) {
	reader := oned.NewCode39Reader()
	return commons.Decode(reader, filename, options...)
}
