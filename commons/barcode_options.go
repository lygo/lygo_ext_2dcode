package commons

type OptionSize struct {
	Width  int
	Height int
}

type OptionCircleShapes struct{}

type OptionImageLogo struct{
	Filename string
}