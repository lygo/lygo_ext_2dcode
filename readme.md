# 2dcode #

![icon](./icon.png)

## Dependencies ##

```
go get -u github.com/yeqown/go-qrcode    
go get -u github.com/makiuchi-d/gozxing
```

## Quick Start ##

Code below generates a QRCODE using circle shapes with a centered logo image.

Note: Circle shapes may be not readable by standard readers.

```go
package main

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_2dcode"
	"bitbucket.org/lygo/lygo_ext_2dcode/commons"
)

func main() {
	generator := lygo_ext_2dcode.NewGenerator(commons.BarcodeFormat_QR_CODE)
	bytes, err := generator.Encode("http://www.gianangelogeminiani.me",
		&commons.OptionSize{Width: 300},
		&commons.OptionCircleShapes{},
		&commons.OptionImageLogo{Filename: "./icon.png"})
	if nil != err {
		panic(err)
	}
	_, err = lygo_io.WriteBytesToFile(bytes, "./qrcode.png")
	if nil != err {
		panic(err)
	}
}
```

## How to Use ##

To use just call:

```
go get -u bitbucket.org/lygo/lygo_ext_2dcode
```

### Versioning ##

Sources are versioned using git tags:

```
git tag v0.1.2
git push origin v0.1.2
```

### Latest Version ##
```
go get bitbucket.org/lygo/lygo_ext_2dcode@v0.1.1
```