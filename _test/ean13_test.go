package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_2dcode"
	"bitbucket.org/lygo/lygo_ext_2dcode/commons"
	"fmt"
	"testing"
)

func TestEan13(t *testing.T) {
	DATA := "1234567890128"
	FORMAT := commons.BarcodeFormat_EAN_13
	FILE := "./ean13.png"


	// GENERATOR
	generator := lygo_ext_2dcode.NewGenerator(FORMAT)
	bytes, err := generator.Encode(DATA,
		&commons.OptionSize{Height: 50})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	_, err = lygo_io.WriteBytesToFile(bytes, FILE)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// SCANNER
	scanner := lygo_ext_2dcode.NewScanner(FORMAT)
	text, err := scanner.Decode(FILE)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if DATA!=text{
		t.Error(fmt.Sprintf("Expected: '%v' got '%v'" , DATA, text))
		t.FailNow()
	}
	fmt.Println(text)
}

