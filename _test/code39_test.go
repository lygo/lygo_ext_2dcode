package _test

import (
	"bitbucket.org/lygo/lygo_commons/lygo_io"
	"bitbucket.org/lygo/lygo_ext_2dcode"
	"bitbucket.org/lygo/lygo_ext_2dcode/commons"
	"fmt"
	"testing"
)

func TestCode39(t *testing.T) {
	DATA := "1234567890123"
	FORMAT := commons.BarcodeFormat_CODE_39
	FORMAT_S := FORMAT.String()
	FILE := "./code39.png"

	// GENERATOR
	bytes, err := lygo_ext_2dcode.Generate(FORMAT_S, DATA,
		&commons.OptionSize{Height: 50})
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	_, err = lygo_io.WriteBytesToFile(bytes, FILE)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// SCANNER
	scanner := lygo_ext_2dcode.NewScanner(FORMAT)
	text, err := scanner.Decode(FILE)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	if DATA!=text{
		t.Error("Expected: " + DATA)
		t.FailNow()
	}
	fmt.Println(text)
}

