module bitbucket.org/lygo/lygo_ext_2dcode

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	github.com/google/uuid v1.3.0 // indirect
	github.com/makiuchi-d/gozxing v0.0.2
	github.com/yeqown/go-qrcode v1.5.8
	golang.org/x/image v0.0.0-20210628002857-a66eb6448b8d // indirect
	golang.org/x/text v0.3.7 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
